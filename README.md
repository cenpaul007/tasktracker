# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Repository contains the code for the web app, "TaskTracker" which records Tasks of various employees.
* Employee - Task Relationship is represented using Django-Rest Framework
* Employees as well as their Tasks can be added, modified, deleted and also can be listed through the 
  respective api end-points.
* Version 1.0

### How do I set up? ###

* **Summary of set up** :

  'requirements.txt' file has included all the dependencies of the app.
  1. `pip install -r requirements.txt `
  2. configure Database in `settings.py`
  3. `python manage.py migrate`
  4. First we need to Create an Employee using employee's api end point 
     (Tasks can't be added without any employees)


* **Dependencies**:

   Google-sheets api has been used to create periodic employee-task report.
   Authentication and required credentials for the same has been included in client_secret.json
   in project's root directory.(It should be moved to an env-file in later stage)
  
   __link to view Task Report_ 
   https://docs.google.com/spreadsheets/d/1sbhfGoQ7TYpWlHXRRXgurkfVgXd6_cZd4zwDaK8HS3s/edit?usp=sharing_
  
  
   Task-generated will be updated on a weekly basis when the management-command 
   `task/management/commands/periodic_task.py` is executed by a cron-job from the respective
   server in which code is being executed.
   
   cron job script:
   `0 0 * * 0  <python_interpreter_path>/python  <project_root>/manage.py  periodic_task`
    Above cron is to execute the code Every Sunday 12AM.
    
    To manually update the task-report-excel:
    `python manage.py periodic_task`
  
* **Database configuration**:
   
   To create a postgres-DB,
  `CREATE DATABASE yourdbname;
   CREATE USER youruser WITH ENCRYPTED PASSWORD 'yourpassword';
   GRANT ALL PRIVILEGES ON DATABASE dbname TO username;`
   
   
   In `settings.py`,
`  DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '<DB_NAME>',
        'USER': '<DB_USER>',
        'PASSWORD': '<********>',
        'HOST': '<HOST>',
        'PORT': <PORT_NO>,
    }
  }`


