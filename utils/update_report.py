import gspread
from oauth2client.service_account import ServiceAccountCredentials
from task.models import Task


def authenticate():
    # use credentials to create a client to interact with the Google Drive API
    scope = ['https://spreadsheets.google.com/feeds', 'https://www.googleapis.com/auth/drive']
    credentials = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', scope)
    client = gspread.authorize(credentials)
    return client


def sheet_update(client, work_book):
    # Find a workbook by name and open the first sheet
    # Make sure you use the right name here.
    sheet = client.open(work_book).sheet1
    # Update Report
    tasks = Task.objects.all().order_by('-created_time')
    ending_row = Task.objects.count() + 2  # adding 2 header rows
    starting_row = 3  # excluding first 2 header rows
    starting_column = 1
    ending_column = 11
    for row_count in range(starting_row, ending_row + 1):
        # iterating through excel sheet row-wise
        task = tasks[row_count - 3]
        for column_count in range(starting_column, ending_column + 1):
            # iterating through excel sheet column-wise
            if column_count == 1:
                sheet.update_cell(row_count, column_count, task.id)
            elif column_count == 2:
                created_time = str(task.created_time.ctime())
                sheet.update_cell(row_count, column_count, created_time)
            elif column_count == 3:
                sheet.update_cell(row_count, column_count, task.task_name)
            elif column_count == 4:
                sheet.update_cell(row_count, column_count, task.status)
            elif column_count == 5:
                sheet.update_cell(row_count, column_count, task.duration)
            elif column_count == 6:
                sheet.update_cell(row_count, column_count, task.start_time)
            elif column_count == 7:
                sheet.update_cell(row_count, column_count, task.end_time)
            elif column_count == 8:
                sheet.update_cell(row_count, column_count, task.employee.id)
            elif column_count == 9:
                sheet.update_cell(row_count, column_count, task.employee.name)
            elif column_count == 10:
                sheet.update_cell(row_count, column_count, task.employee.role)
            elif column_count == 11:
                task_count = task.employee.tasks.count()
                sheet.update_cell(row_count, column_count, task_count)
            else:
                return
    return
