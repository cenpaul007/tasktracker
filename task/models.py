from django.db import models


class Employee(models.Model):
    """Employee extends django's user model -> to store employee details"""
    name = models.CharField(max_length=30, null=False)
    role = models.CharField(max_length=30, null=False)

    class Meta:
        verbose_name = "Employee"

    def __str__(self):
        return "%s%s" % (self.name, self.role)


class Task(models.Model):
    """Task model to store the task details"""
    employee = models.ForeignKey(Employee, related_name='tasks', on_delete=models.CASCADE)
    task_name = models.CharField(max_length=30, null=False)
    duration = models.CharField(max_length=30)
    status = models.CharField(max_length=30, null=True)
    start_time = models.CharField(max_length=30, verbose_name='Start Time', null=False)
    end_time = models.CharField(max_length=30, verbose_name='End Time', null=False)
    created_time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "%s" % self.task_name
