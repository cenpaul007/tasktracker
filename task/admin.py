from django.contrib import admin
from task.models import Employee, Task

admin.site.register(Employee)
admin.site.register(Task)
