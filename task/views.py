from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from task.models import Task, Employee
from task.serializers import TaskSerializer, EmployeeSerializer


class TaskViewSet(viewsets.ModelViewSet):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer


class EmployeeViewSet(viewsets.ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
