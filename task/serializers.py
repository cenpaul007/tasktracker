from django.contrib.auth.models import User, Group
from rest_framework import serializers
from task.models import Task, Employee


class EmployeeSerializer(serializers.HyperlinkedModelSerializer):
    tasks = serializers.HyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='task:task-detail'
    )

    class Meta:
        model = Employee
        fields = ['name', 'role', 'tasks', 'url']
        extra_kwargs = {
            'url': {'view_name': 'task:employee-detail'},
        }


class TaskSerializer(serializers.HyperlinkedModelSerializer):

    employee = serializers.HyperlinkedRelatedField(
        many=False,
        required=True,
        queryset=Employee.objects.all(),
        view_name='task:employee-detail',
    )
    task = serializers.CharField(source='task_name')

    class Meta:
        model = Task
        fields = ['employee', 'task', 'status', 'duration', 'start_time', 'end_time', 'url']
        extra_kwargs = {
            'url': {'view_name': 'task:task-detail'},
        }




