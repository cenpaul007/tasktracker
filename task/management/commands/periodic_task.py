from utils.update_report import authenticate, sheet_update
from django.core.management.base import BaseCommand, CommandError


def generate_report():
    work_book_name = "TaskTrackerReport"  # Name of the excel file in cloud
    sheet_client = authenticate()  # api authentication
    sheet_update(sheet_client, work_book_name)


class Command(BaseCommand):
    help = "This command will be executed periodically as configured in system's cron"

    def handle(self, *args, **options):
        return generate_report()
