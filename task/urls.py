from django.urls import path, include
from rest_framework import routers
from task import views

app_name = 'task'
router = routers.DefaultRouter()
router.register('task', views.TaskViewSet)
router.register('employee', views.EmployeeViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
